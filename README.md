# TYPO3 Extension ``be_groups``
## Beschreibung
TYPO3 Rechte-Management für Backend-Benutzergruppen.

## ToDo
* hide_in_lists entfernen: https://docs.typo3.org/c/typo3/cms-core/main/en-us/Changelog/9.0/Breaking-81534-DatabaseFieldBe_groupshide_in_listsDropped.html#breaking-81534-database-field-be-groups-hide-in-lists-dropped
* Code aufräumen (Ursprungscode ist von TYPO8)

## Community


## Autoren und Danksagung
Das eigentliche Projekt wurde von der AOE GmbH entwickelt. Federführend für die Entwicklung waren folgende Entwickler:
 * Michael Klapper <michael.klapper@aoe.com>
 * Tomas Norre Mikkelsen <tomas.mikkelsen@aoe.com>
Ich habe die Extension während meines Praktikums bei AOE GmbH im Einsatz gesehen und fand es schade, dass sie nicht veröffentlicht wurde. Auf der Suche nach einer ähnlichen Extension bin ich auf https://github.com/AOEpeople/be_groups über den nun öffentlich zugänglichen Quellcode gestolpert und habe versuche die Extension für TYPO3 11 zu reaktivieren.


## Lizenz
Das original Projekt wurde unter der "GPL-2.0+" veröffentlicht und wird von mir unter der "GNU General Public License version 3" gestellt.